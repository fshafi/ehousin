<?php
/**
 * WordPress Ajax Process Execution
 *
 * @package WordPress
 * @subpackage Administration
 *
 * @link https://codex.wordpress.org/AJAX_in_Plugins
 */

/**
 * Executing Ajax process.
 *
 * @since 2.1.0
 */
define( 'DOING_AJAX', true );
if ( ! defined( 'WP_ADMIN' ) ) {
    define( 'WP_ADMIN', true );
}

/** Load WordPress Bootstrap */
require_once( dirname( dirname( __FILE__ ) ) . '/wp-load.php' );

/** Allow for cross-domain requests (from the front end). */
send_origin_headers();

// Require an action parameter
if ( empty( $_REQUEST['action'] ) )
    die( '0' );

/** Load WordPress Administration APIs */
require_once( ABSPATH . 'wp-admin/includes/admin.php' );

/** Load Ajax Handlers for WordPress Core */
require_once( ABSPATH . 'wp-admin/includes/ajax-actions.php' );

@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
@header( 'X-Robots-Tag: noindex' );

send_nosniff_header();
nocache_headers();



/** This action is documented in wp-admin/admin.php */
do_action( 'admin_init' );

$core_actions_get = array(
    'fetch-list', 'ajax-tag-search', 'wp-compression-test', 'imgedit-preview', 'oembed-cache',
    'autocomplete-user', 'dashboard-widgets', 'logged-in',
);

$core_actions_post = array(
    'oembed-cache', 'image-editor', 'delete-comment', 'delete-tag', 'delete-link',
    'delete-meta', 'delete-post', 'trash-post', 'untrash-post', 'delete-page', 'dim-comment',
    'add-link-category', 'add-tag', 'get-tagcloud', 'get-comments', 'replyto-comment',
    'edit-comment', 'add-menu-item', 'add-meta', 'add-user', 'closed-postboxes',
    'hidden-columns', 'update-welcome-panel', 'menu-get-metabox', 'wp-link-ajax',
    'menu-locations-save', 'menu-quick-search', 'meta-box-order', 'get-permalink',
    'sample-permalink', 'inline-save', 'inline-save-tax', 'find_posts', 'widgets-order',
    'save-widget', 'delete-inactive-widgets', 'set-post-thumbnail', 'date_format', 'time_format',
    'wp-remove-post-lock', 'dismiss-wp-pointer', 'upload-attachment', 'get-attachment',
    'query-attachments', 'save-attachment', 'save-attachment-compat', 'send-link-to-editor',
    'send-attachment-to-editor', 'save-attachment-order', 'heartbeat', 'get-revision-diffs',
    'save-user-color-scheme', 'update-widget', 'query-themes', 'parse-embed', 'set-attachment-thumbnail',
    'parse-media-shortcode', 'destroy-sessions', 'install-plugin', 'update-plugin', 'press-this-save-post',
    'press-this-add-category', 'crop-image', 'generate-password', 'save-wporg-username', 'delete-plugin',
    'search-plugins', 'search-install-plugins', 'activate-plugin', 'update-theme', 'delete-theme',
    'install-theme', 'get-post-thumbnail-html',
);

add_action( 'wp_ajax_registration', 'registration' );
add_action( 'wp_ajax_nopriv_registration', 'registration' );
function registration() {

    $response = new StdClass;
    $response->status = 'failure';
    if ( empty ( $_POST['user_email'] ) || empty ( $_POST['user_password'] ) || empty ( $_POST['user_phone'] ) || empty ( $_POST['user_name'] ) )
    {
        $response->message = 'All fields required!';
    }
    else
    {
        $phone=$_POST['user_phone'];

        $user_data = array(
            'user_login' => $_POST['user_email'],
            'display_name' =>$_POST['user_name'],
            'first_name' =>$_POST['user_name'],
            'user_pass'  => $_POST['user_password'],
            'user_email' =>$_POST['user_email'],
            'role'       => 'unapproved'
        );
        $user_id = wp_insert_user( $user_data );
        
        if( is_wp_error( $user_id ) ) {
            $response->message = 'Email already exists!';
        } else {
            add_user_meta( $user_id, 'phone', $phone);
            add_user_meta( $user_id, 'pass', $_POST['user_password']);
            do_action('user_register', $user_id);

            $response->status = 'success';
            $response->data->id =$user_id;
            $response->data->name =$_POST['user_name'] ;
            $response->data->email =$_POST['user_email'];;
            $response->data->phone = $phone;
            $response->data->user_status=array('unapproved');

        }
    }

    header('Content-Type: application/json');
    echo json_encode($response);
    exit();
}

add_action( 'wp_ajax_login', 'login' );
add_action( 'wp_ajax_nopriv_login', 'login' );
function login() {

    $response = new StdClass;
    $response->status = 'failure';
    if ( empty ( $_POST['user_login'] ) || empty ( $_POST['user_password'] ) ) {
        $response->message = 'user_login and user_password both required';
    } else {

        $user = wp_signon( $_POST );

        if ( is_wp_error( $user ) ) {
            $response->message = 'Invalid user_login OR user_password';
        } else {
            $response->status = 'success';
            $response->data->id =$user->id;
            $response->data->name =$user->first_name ;
            $response->data->email = $user->user_login;
            $response->data->phone = $user->phone;
            $response->data->user_status = $user->roles;

        }
    }

    header('Content-Type: application/json');
    echo json_encode($response);
    exit();
}

add_action('wp_ajax_set_password','set_password');
add_action('wp_ajax_nopriv_set_password','set_password');
function set_password(){

    $response = new StdClass;
    $response->status = 'failure';
    if ( empty ( $_POST['user_id'] ) || empty ( $_POST['user_password'] ) ) {
        $response->message = 'user_id and user_password both required';
    }
    else{
        $user_id = $_POST['user_id'];
        $password = $_POST['user_password'];
        $user = get_userdata( $user_id );
        if ( $user === false ) {
            $response->message = 'Invalid ID';
        } else {
            wp_set_password( $password, $user_id );
            update_user_meta($user_id, 'pass', $password);

            $response->status = 'success';
            $response->message = 'Password set successfully';

        }
    }

    header('Content-Type: application/json');
    echo json_encode($response);
    exit();

}

add_action('wp_ajax_forgot_password','forgot_password');
add_action('wp_ajax_nopriv_forgot_password','forgot_password');
function forgot_password(){
    $response = new StdClass;
    $response->status = 'failure';
    if ( empty ( $_POST['user_email'] ) ) {
        $response->message = 'user_email is required';
    }
    else{
        $email = $_POST['user_email'];
        $exists = email_exists($email);
        if ( $exists ){
            $key = 'pass';
            $single = true;
            $user_pass = get_user_meta( $exists, $key, $single );
            $subject = 'Forgot Password';
            $message = 'Your application password is: '.$user_pass;
            wp_mail( $email, $subject, $message );
            $response->status = 'success';
            $response->message = 'Check your registered email for password';
            
        }
    else
    {
        $response->message = "That E-mail doesn't belong to any registered users on this application";
    }
          
    }
    header('Content-Type: application/json');
    echo json_encode($response);
    exit();
}

add_action('wp_ajax_regions','regions');
add_action('wp_ajax_nopriv_regions','regions');
function regions(){
    global $wpdb;
    $regions = $wpdb->get_results("SELECT * FROM wp_ehousingregions;");
    $response = new StdClass;
    $response->status = 'success';
    $response->data=$regions;
    //print_r($customers);
    header('Content-Type: application/json');
    echo json_encode($response);
    exit();
}

add_action('wp_ajax_topics','topics');
add_action('wp_ajax_nopriv_topics','topics');
function topics(){
    global $wpdb;
    $topics = $wpdb->get_results("SELECT * FROM wp_ehousingtopics;");
    $response = new StdClass;
    $response->status = 'success';
    $response->data=$topics;

    header('Content-Type: application/json');
    echo json_encode($response);
    exit();
}

add_action('wp_ajax_get_region_country','get_region_country');
add_action('wp_ajax_nopriv_get_region_country','get_region_country');

function get_region_country()
{
    global $wpdb;
    $topics = $wpdb->get_results("SELECT * FROM wp_ehousincountries LEFT JOIN wp_ehousingregions on wp_ehousingregions.r_id =wp_ehousincountries.region_id;");

    foreach($topics as $topic){
        if($topic->region_id==1){

            $the_americas[]=array('country_name'=>$topic->country_name,'country_name_arabic'=>$topic->country_name_arabic);
            $region_name_the_americas=$topic->region_name;
            $region_name_arabic_the_americas=$topic->region_name_arabic;
        }
        else if($topic->region_id==2){
            $asia[]=array('country_name'=>$topic->country_name,'country_name_arabic'=>$topic->country_name_arabic);
            $region_name_asia=$topic->region_name;
            $region_name_arabic_asia=$topic->region_name_arabic;
        }
        else if($topic->region_id==3){
            $europe[]=array('country_name'=>$topic->country_name,'country_name_arabic'=>$topic->country_name_arabic);
            $region_name_europe=$topic->region_name;
            $region_name_arabic_europe=$topic->region_name_arabic;
        }
        else if($topic->region_id==4){
            $Former_Soviet_Union[]=array('country_name'=>$topic->country_name,'country_name_arabic'=>$topic->country_name_arabic);
            $region_name_Former_Soviet_Union=$topic->region_name;
            $region_name_arabic_Former_Soviet_Union=$topic->region_name_arabic;
        }
        else if($topic->region_id==5){
            $midle_east[]=array('country_name'=>$topic->country_name,'country_name_arabic'=>$topic->country_name_arabic);
            $region_name_midle_east=$topic->region_name;
            $region_name_arabic_midle_east=$topic->region_name_arabic;
        }
        else if($topic->region_id==6){
            $north_africa[]=array('country_name'=>$topic->country_name,'country_name_arabic'=>$topic->country_name_arabic);
            $region_name_north_africa=$topic->region_name;
            $region_name_arabic_north_africa=$topic->region_name_arabic;
        }
        else if($topic->region_id==7){
            $africa[]=array('country_name'=>$topic->country_name,'country_name_arabic'=>$topic->country_name_arabic);
            $region_name_africa=$topic->region_name;
            $region_name_arabic_africa=$topic->region_name_arabic;
        }

    }
    $the_americas=array('region_name_english'=>$region_name_the_americas,'region_name_arabic'=>$region_name_arabic_the_americas,'countries_list'=>$the_americas);
    $asia=array('region_name_english'=>$region_name_asia,'region_name_arabic'=>$region_name_arabic_asia,'countries_list'=>$asia);
    $europe=array('region_name_english'=>$region_name_europe,'region_name_arabic'=>$region_name_arabic_europe,'countries_list'=>$europe);
    $Former_Soviet_Union=array('region_name_english'=>$region_name_Former_Soviet_Union,'region_name_arabic'=>$region_name_arabic_Former_Soviet_Union,'countries_list'=>$Former_Soviet_Union);
    $midle_east=array('region_name_english'=>$region_name_midle_east,'region_name_arabic'=>$region_name_arabic_midle_east,'countries_list'=>$midle_east);
    $north_africa=array('region_name_english'=>$region_name_north_africa,'region_name_arabic'=>$region_name_arabic_north_africa,'countries_list'=>$north_africa);
    $africa=array('region_name_english'=>$region_name_africa,'region_name_arabic'=>$region_name_arabic_africa,'countries_list'=>$africa);


    $datas=array($the_americas,$asia,$europe,$Former_Soviet_Union,$midle_east,$north_africa,$africa);

    header('Content-Type: application/json');
    $data=array('status'=>'success','data'=>$datas);
    echo json_encode($data);
    exit();


}


add_action('wp_ajax_test','test');
add_action('wp_ajax_nopriv_test','test');
function test(){

    $super_admins = get_super_admins();
    echo 'List of super-admin users:<ul>';
    foreach ($super_admins as $admin) {
        echo '<li>' . $admin . '</li>';
    }
    echo '</ul>';
    exit();

}

// Deprecated
$core_actions_post[] = 'wp-fullscreen-save-post';

// Register core Ajax calls.
if ( ! empty( $_GET['action'] ) && in_array( $_GET['action'], $core_actions_get ) )
    add_action( 'wp_ajax_' . $_GET['action'], 'wp_ajax_' . str_replace( '-', '_', $_GET['action'] ), 1 );

if ( ! empty( $_POST['action'] ) && in_array( $_POST['action'], $core_actions_post ) )
    add_action( 'wp_ajax_' . $_POST['action'], 'wp_ajax_' . str_replace( '-', '_', $_POST['action'] ), 1 );

add_action( 'wp_ajax_nopriv_heartbeat', 'wp_ajax_nopriv_heartbeat', 1 );

if ( is_user_logged_in() ) {
    /**
     * Fires authenticated Ajax actions for logged-in users.
     *
     * The dynamic portion of the hook name, `$_REQUEST['action']`,
     * refers to the name of the Ajax action callback being fired.
     *
     * @since 2.1.0
     */
    do_action( 'wp_ajax_' . $_REQUEST['action'] );
} else {
    /**
     * Fires non-authenticated Ajax actions for logged-out users.
     *
     * The dynamic portion of the hook name, `$_REQUEST['action']`,
     * refers to the name of the Ajax action callback being fired.
     *
     * @since 2.8.0
     */
    do_action( 'wp_ajax_nopriv_' . $_REQUEST['action'] );
}


// Default status
die( '0' );
